# -*- coding: utf8 -*-
import os
import sys
import argparse
import yaml
from ..customlogging import customlogging as log


class Arguments(object):
    """Creates arguments for passing files and configuration data to the script.

    A Class which configures and retrieves Arguments for use by a python script. Dictionaries
    retrieved from YAML files are added as attributes to this class with no sub-processing.
    Information including the filename of each YAML file parsed is also added as an attribute.

    See :ref:`arguments` for more details.

    """
    def __init__(self):
        if os.path.exists('../../logging.yml'):
            logger_yml_file = '../../logging.yml'
        elif sys.platform.lower() == 'linux':
            logger_yml_file = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                                           '../../../../../xml2vhdl-ox/logging.yml'))

        else:
            logger_yml_file = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                                           '../../../../xml2vhdl-ox/logging.yml'))

        self.logger = log.config_logger(name=__name__, class_name=self.__class__.__name__,
                                        logr_yml=logger_yml_file,
                                        default_level=log.logging.INFO)
        _parser = self._create_parser()
        self._add_arguments(_parser)
        args = self._get_args(_parser)
        for k in vars(args):
            self.logger.debug('Processed Argument: --{arg}'
                              .format(arg=k))
        self.logger.debug('Loading Arguments from Command Line...')
        self._import_from_yaml(args)

    def __repr__(self):
        return "{self.__class__.__name__}()".format(self=self)

    def __str__(self):
        return "{name} is a {self.__class__.__name__} object".format(name=__name__, self=self)

    @staticmethod
    def _create_parser():
        """Creates an ``argparse`` object

        Returns:
            :obj:`argparse.ArgumentParser()`
        """
        return argparse.ArgumentParser()

    @staticmethod
    def _add_arguments(parser):
        """Adds Arguments to provide to python script, using the argparse module.

        Args:
            parser (obj): argparse object

        Returns:
            None

        """
        parser.add_argument("-l", "--log",
                            dest="log",
                            action="store_true",
                            help="Print version and history log.")
        parser.add_argument("-i", "--input_file",
                            dest="input_file",
                            action="append",
                            default=list(),
                            help="XML input files. It is possible to specify several files by repeating \
                                    this option.")
        parser.add_argument("-d", "--input_folder",
                            dest="input_folder",
                            action="append",
                            default=list(),
                            help="Paths to input files folders. It is possible to specify several folders by \
                                    repeating this option. The script runs on all XML files in included \
                                    folders.")
        parser.add_argument("-p", "--path",
                            dest="path",
                            action="append",
                            default=list(),
                            help="Paths to linked XML files. It is possible to specify multiple paths by \
                                    repeating this option.")
        parser.add_argument("-v", "--vhdl_output",
                            dest="vhdl_output",
                            default="",
                            help="Generated VHDL output folder.")
        parser.add_argument("-x", "--xml_output",
                            dest="xml_output",
                            default="",
                            help="Generated XML output folder.")
        parser.add_argument("-b", "--bus_library",
                            dest="bus_library",
                            default="work",
                            help="Generated code will reference the specified bus VHDL library. \
                                    Default is: 'work'.")
        parser.add_argument("-s", "--slave_library",
                            dest="slave_library",
                            default="work",
                            help="Generated code will reference the specified slave VHDL library. \
                                    Default is: 'work'.")
        parser.add_argument("-n", "--bus_definition_number",
                            dest="bus_definition_number",
                            default="0",
                            help="Bus definition index. \
                                    '0' = axi4lite(32bit) - standard [default], \
                                    '1' = axi4lite(32bit) - casperFPGA, \
                                    '2' = wishbone(16bit), \
                                    '3' = wishbone(32bit)")
        parser.add_argument("--relative_output_path",
                            dest="relative_output_path",
                            default="",
                            help="Use a relative output path, to input XML file, path for the output folder, \
                                    it overrides '-v' ('--vhdl_output') and '-x' ('--xml_output) options")
        parser.add_argument("--board_xml_file",
                            dest="board_xml_file",
                            default="",
                            help="The name of top level (usually board level) xml file to generate an Altera \
                                    System Console TCL GUI file for. This feature is ignored if left blank")
        parser.add_argument("--gen_tb_packages",
                            dest="gen_tb_packages",
                            default=True,
                            help="Whether to generate VHDL Packages for the  resolvedMemory Maps which approximate \
                                    a user Axi4lite Interface To Use in Simulation Test-benches")
        parser.add_argument("--tb_package_exclude_list",
                            dest="tb_package_exclude_list",
                            default=list(),
                            help="A List of Terms to exclude from VHDL Test-Bench Packages to prevent bloated files")
        # Specific for Slaves:
        parser.add_argument("-a", "--attributes",
                            dest="xml_help",
                            action="store_true",
                            help="Prints supported XML attributes and exits.")
        parser.add_argument("--relocate_path",
                            dest="relocate_path",
                            default="",
                            help="Relocate BRAM init file absolute path. Example: \
                                    --relocate_path 'c:\\->d:\\work'")
        parser.add_argument("-c", "--constant",
                            dest="constant",
                            action="append",
                            default=list(),
                            help="Constant passed to script. The script will substitute the specified \
                                    constant value in the XML file. Example '-c constant1=0x1234'. ")
        parser.add_argument("--tb",
                            dest="tb",
                            action="store_true",
                            help="Generate a simple test bench in the 'sim' sub-folder.")
        # Specific for ICs
        parser.add_argument("-t", "--top",
                            dest="vhdl_top",
                            default="",
                            help="Top Level IC for VHDL generation. If this option is not specified VHDL \
                                    is generated for the root node IC.")
        parser.add_argument("-r", "--vhdl_record_name",
                            dest="vhdl_record_name",
                            default="t_axi4lite_mmap_slaves",
                            help="VHDL record name containing slaves memory map. \
                                    Default is: 't_axi4lite_mmap_slaves'.")
        parser.add_argument("--zip",
                            dest="zip",
                            action="store_true",
                            help="Only generate a BRAM Hex init file containing the zip compressed \
                                    XML input file")

    @staticmethod
    def _get_args(parser):
        """Fetches the arguments from a argparse object

        Args:
            parser (:obj:`object`): ``argparse`` object

        """
        args = parser.parse_args()
        return args

    def _import_from_yaml(self, args):
        """Imports attributes from ``yml`` file.

        Imports attributes from YAML Files defined by ``_ARG_FILELIST`` which have been passed to via
        Argument(s) attribute names match the corresponding arg name, and a reference containing the
        absolute path to the YAML file is also stored in an attribute named ``<arg>_file``

        Args:
            config (:obj:`dict`):

        """
        for arg in vars(args):
            attr = getattr(args, arg)
            if hasattr(attr, 'read'):
                name_attr = '{arg}'.format(arg=arg)
                fname_attr = '{arg}_file'.format(arg=arg)
                self.logger.debug('Processing:                   --{attr}'
                                  .format(attr=name_attr))
                self.logger.debug('File-path Reference Attribute: {attr}'
                                  .format(attr=fname_attr))
                try:
                    abspath = os.path.abspath(attr.name)
                    contents = yaml.load(attr, Loader=yaml.SafeLoader)
                    attr.close()
                    setattr(self, fname_attr, abspath)
                    self.logger.debug('Path:                         {path}'
                                      .format(path=abspath))
                    setattr(self, name_attr, contents)
                except AttributeError as e:
                    self.logger.error('Error Importing {attr} YAML File from Argument. File '
                                      '(Probably) Not a YAML File'
                                      .format(attr=fname_attr))
                    self.logger.error('\t{}.'
                                      .format(e))
                    log.errorexit(self.logger)
            else:
                self.logger.debug('Processing:                   --{arg}'
                                  .format(arg=arg))
                setattr(self, arg, getattr(args, arg))
