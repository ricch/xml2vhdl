# -*- coding: utf8 -*-
__author__ = 'Riccardo Chiello'
__all__ = ['Xml2VhdlGenerate', 'Xml2Vhdl']
from .xml2vhdl import Xml2Vhdl, Xml2VhdlGenerate
